// JavaScript
let lastScrollTop = 0;
const header = document.getElementById('header');
const banner = document.querySelector('.banner');

window.addEventListener("scroll", function() {
  let currentScroll = window.pageYOffset || document.documentElement.scrollTop;
  const threshold = 100;

  if (currentScroll > threshold) {
    header.classList.add('hidden');
    logo.src = newLogoURL;
  } else {
    header.classList.remove('hidden');
    header.classList.add('scrolled-up');
    logo.src = 'https://suitmedia.com/_nuxt/img/logo.6c17d4a.png'; 
  }
  lastScrollTop = currentScroll <= 0 ? 0 : currentScroll;
}, false);

window.onload = function() {
  logo.src = 'img/logo.png';
};

function loadPosts(pageNumber, pageSize) {
  }
  function addPostToDOM(post) {
    const postList = document.getElementById('postList');
  }
  
  document.getElementById('loadMoreBtn').addEventListener('click', () => {
  });
  
  document.getElementById('sort').addEventListener('change', () => {
  });
  
  document.getElementById('perPage').addEventListener('change', () => {
  });
  
  loadPosts(1, 10);

function fetchIdeas(pageNumber, pageSize, sortBy) {
}

document.getElementById('sort').addEventListener('change', () => {
});

document.getElementById('perPage').addEventListener('change', () => {
});

document.getElementById('loadMoreBtn').addEventListener('click', () => {
});

fetchIdeas(1, 10, '-published_at');